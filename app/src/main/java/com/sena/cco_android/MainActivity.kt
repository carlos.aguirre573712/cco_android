package com.sena.cco_android

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.appcompat.widget.AppCompatButton
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.sena.cco_android.login.LoginActivity
import com.sena.cco_android.ui.theme.Cco_androidTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_main)

        val btnStartLogin = findViewById<AppCompatButton>(R.id.button1)


        btnStartLogin.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
//            intent.putExtra("EXTRA_NAME", name)
            startActivity(intent)
        }
    }

}